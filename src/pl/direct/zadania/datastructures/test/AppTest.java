package pl.direct.zadania.datastructures.test;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import pl.direct.zadania.datastructures.model.ProductAttribute;
import pl.direct.zadania.datastructures.model.ProductAttributeComponent;
import pl.direct.zadania.datastructures.model.ProductAttributeGroup;

class AppTest {

	static ProductAttributeComponent pg1 = new ProductAttributeGroup("Product Attribute Group 1", new BigDecimal(0));
	static ProductAttributeComponent pg2 = new ProductAttributeGroup("Product Attribute Group 2", new BigDecimal(0));
	static ProductAttributeComponent pg3 = new ProductAttributeGroup("Product Attribute Group 3", new BigDecimal(0));
	
	static ProductAttributeComponent pg4 = new ProductAttributeGroup("All Product Attribute Groups", new BigDecimal(0));
	
	static ProductAttributeComponent pa1 = new ProductAttribute("Product Attribute YYY", new BigDecimal(5));
	static ProductAttributeComponent pa2 = new ProductAttribute("Product Attribute ZZZ", new BigDecimal(10));
	static ProductAttributeComponent pa3 = new ProductAttribute("Product Attribute XXX", new BigDecimal(10));
	static ProductAttributeComponent pa4 = new ProductAttribute("Product Attribute AAA", new BigDecimal(10));
	
	static ProductAttributeComponent pa5 = new ProductAttribute("Product Attribute 333", new BigDecimal(10));
	static ProductAttributeComponent pa6 = new ProductAttribute("Product Attribute 555", new BigDecimal(10));
	
	static ProductAttributeComponent pa7outsider = new ProductAttribute("Product Attribute ???", new BigDecimal(10));
	
	@BeforeAll
	public static void setup() {
		
		pg1 = Mockito.spy(pg1);
		pa1 = Mockito.spy(pa1);
		pa7outsider = Mockito.spy(pa7outsider);
		
		pg1.add(pa1);
		pg1.add(pa2);
		pg1.add(pa3);
		pg2.add(pa4);
		pg4.add(pg1);
		pg4.add(pg2);
		pg4.add(pg3);
		pg3.add(pa5);
		pg3.add(pa6);
		pg2.add(pg3);
		
		pg4.print();
	}
	
	
	@Test
	void shouldCallPrintOnGroupComponent() {
		verify(pg1,times(1)).print();
	}
	
	@Test
	void shouldCallPrintOnSingleComponent() {
		verify(pa1,atLeastOnce()).print();
	}
	
	@Test
	void shouldNotCallPrintOnSingleComponentNotInHierarhy() {
		verify(pa7outsider,times(0)).print();
	}


}































