package pl.direct.zadania.datastructures.model;

import java.math.BigDecimal;

public abstract class ProductAttributeComponent {
	
	
	public void add(ProductAttributeComponent pAttr) {
		throw new UnsupportedOperationException();
	}
	
	public void remove(ProductAttributeComponent pAttr) {
		throw new UnsupportedOperationException();
	}
	
	public ProductAttributeComponent getChild(int i) {
		throw new UnsupportedOperationException();
	}
	
	public String getName() {
		throw new UnsupportedOperationException();
	}
	
	public BigDecimal getPrice() {
		throw new UnsupportedOperationException();
	}
	
	public void print() {
		throw new UnsupportedOperationException();
	}
}
