package pl.direct.zadania.datastructures.model;

import java.math.BigDecimal;

public class KitchenTable extends ProductAttributeComponent{
	
	private BigDecimal price;
	
	private String name;
	
	private int width;
	
	private int depth;
	
	private int height;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
