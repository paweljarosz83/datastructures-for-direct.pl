package pl.direct.zadania.datastructures.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

public class ProductAttributeGroup extends ProductAttributeComponent{
	
	String name;
	BigDecimal price;
	
	ArrayList<ProductAttributeComponent> productAttributeComponents = new ArrayList<>();
	
	public ProductAttributeGroup(String name,BigDecimal price) {
		this.name = name;
		this.price = price;
	}

	@Override
	public void add(ProductAttributeComponent pAttr) {
		productAttributeComponents.add(pAttr);
	}

	@Override
	public void remove(ProductAttributeComponent pAttr) {
		productAttributeComponents.remove(pAttr);
	}

	@Override
	public ProductAttributeComponent getChild(int i) {
		return (ProductAttributeComponent) productAttributeComponents.get(i);
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public String getName() {
		return name;
	}

	public void print() {
		
	
		System.out.print(" \n Name: "+name);
		System.out.println(", "+price);
	

		Iterator<ProductAttributeComponent> iterator = productAttributeComponents.iterator();
		
		while(iterator.hasNext()) {
			ProductAttributeComponent pac = (ProductAttributeComponent)iterator.next();
			pac.print();
		}
		System.out.println("---------------");
	}

	
	

}
