package pl.direct.zadania.datastructures.model;

import java.math.BigDecimal;

public class Bike extends ProductAttributeComponent{

	private BigDecimal price;
	
	private String name;
	
	private String color;
	
	private int size;

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
